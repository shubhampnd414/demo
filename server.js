var express=require("express");
const app=express();
const blog=require("./models/blogmodel")
app.set("view engine","ejs");
var bodyParser=require("body-parser");
const { all } = require("async");
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static('public'));
var mongo=require("mongoose");
const { schema } = require("./models/blogmodel");
mongo.connect("mongodb://localhost/Blogs");
const blogSchema=new mongo.Schema({
    title:{
        required:true,
        type:String
    },
    category:{
        type:String 
    },
    content:{
        required:true,
        type:String
    },
    createdAt:{
        type:Date,
        default:Date.now
    }
})
var model=mongo.model("blogs",blogSchema)
model.create({
    title:"new one",
    category:"new",
    content:"new ha yr"
})
//blog schema
/*
var blogSchema=new mongo.Schema({
    title:String ,
    createAt:Date,
    
    categories:[],
    content:String
})*/

//blog model
//var blog=mongo.model("Blog",blogSchema);

//demo blogs
var allblogs=[
    {title:"harry",category:"movie",content:"dcdjcndjn"}
]
app.get("/blog/Allblogs",(req,res)=>{
    model.find({},(err,blogs)=>{
        if(err)
        {
            console.log(err)
        }
        else{
            res.render("blogs",{blogs:blogs})
        }
    })
})



//get new post 
app.get("/blog/create",(req,res)=>{
    res.render("create");
})

// post new post
app.post("/blog/create",(req,res)=>{
model.create({
    title:req.body.blog_title,
    category:req.body.blog_category,
    content:req.body.blog_content,
    
},(err,current)=>{
    if(err)
    {
        console.log(err)
    }
    else{
        console.log(current)
    }
})

res.redirect("Allblogs");
console.log(body);
res.render("blogs",{allblogs:blog})
})


//Blog detail
app.get("/blog/:id",(req,res)=>{
const blogId=req.params.id;
//get the specfic blog 
var myblog=model.findById(blogId,(err,blog)=>{
    res.render("Detail",{blog:blog})
})
})


//Edit blog-Get

app.get("/blog/:id/edit",(req,res)=>{
    model.findById(req.params.id,(err,myblog)=>{
        if(!err)
        {
            res.render("edit",{blog:myblog})
        }
    })
})

//Edit  blog-Post
app.post("/blog/:id",(req,res)=>{
    let blogUpdate={
        title:req.body.title,
        category:req.body.blog_category,
        content:req.body.blog_content
    }
    console.log(req.body.blog_content)
    model.findByIdAndUpdate(req.params.id,blogUpdate,(err,updatedBlog)=>{
   console.log(updatedBlog)
    })
})

app.listen(3000,(err)=>{
    if(!err)
    {
        console.log("server is fine!!!");
        }
})