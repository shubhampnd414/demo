var mongoose=require("mongoose");
const blogSchema=new mongoose.Schema({
    title:{
        required:true,
        type:String
    },
    category:{
        type:String 
    },
    content:{
        required:true,
        type:String
    },
    createdAt:{
        type:Date,
        default:Date.now
    }
})

///module.exports=mongoose.model("bb",blogSchema)